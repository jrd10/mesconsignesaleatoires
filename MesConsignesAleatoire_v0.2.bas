﻿Option VBASupport 1

REM  *****  BASIC  *****
' Mes Consignes Aléatoires v0,2
' Update: August 8, 2022
' 
' Table is now movable (except buttons) thank to "Cell position by name",
' Iteration data reset button
' Stop button
' Number of iterration
'


' Declaration ==========================

' Initial parameters ---------------------------------------------------------------------
' Important: Cell names have to be declared in the sheet

Public TimeScale							As Variant		' Rafale or secondes... 1 to 4 (Set Content of linked Cell: as Position Entry)
Public WaitScale							As Integer		' Scale duration in secons (0 (Rafale), 1, 60, 3600)
Public TimeStepValue					As Variant		' Seconde... number
Public InitialSetPoint					As Variant		' Initial consigne value
Public ThresholdMaxValue			As Variant		' High Threshold
Public ThresholdMinValue			As Variant		' Low Threshold
Public IterationNumber				As Variant		' Number of iteration: 


' Variables for calculation ---------------------------------------------------------------
' Important: Cell names have to be declared in the sheet

Public CalculationTime 				As Variant		' Script starts a this time
Public RandomValueIteration		As Variant		' Random value of each iteration iteration, 
Public TimeStepIteration			As Variant		' Time step (secondes...) itération 
Public MaxThresholdIteration		As Variant		' Max threshold itération for graphics
Public MinThresholdIteration		As Variant		' Min threshold itération for graphics
Public SetPointIteration				As Variant		' Set point itération


' Sheet object declaration
Dim oSheet 								As Object


' =============================================
Sub Main()


' Sheet object activation
oSheet = ThisComponent.CurrentController.getActiveSheet()

' Calculation thresold by the "Automatique" button

' Parameter acquisition according to Cell name --------------------------------------------------
' Get value with: variableName 	= oSheet.getCellRangeByName("CellName").value
' You can move the named cell without impact on the script

TimeScale					= oSheet.getCellRangeByName("TimeScale").value					' Rafale, secondes... getCellByPosition(10, 4)

TimeStepValue			= oSheet.getCellRangeByName("TimeStepValue").value				' number of seconde...

InitialSetPoint				= oSheet.getCellRangeByName("InitialSetPoint").value				' Initial Set Point

ThresholdMaxValue		= oSheet.getCellRangeByName("ThresholdMaxValue").value		' Threshold Max Value

ThresholdMinValue		= oSheet.getCellRangeByName("ThresholdMinValue").value		' Threshold Min Value

' IterationNumber, Number of iteration : see RAZIteration function


' Record Calculation time in the named Cell

CalculationTime		= Now
oSheet.getCellRangeByName("CalculationTime").String		= CalculationTime	


' Erase all the iteration Can be simplified with a table

	RAZIteration

' Calculation iteration ===================================== 
 ' Write new value
	 For i = 0 to  IterationNumber -1 

		' Random Value between max and min threshold (min as to be negative) 					= Round((rnd * (max-min)) + min)  
		RandomValue					= Round((rnd * (ThresholdMaxValue - ThresholdMinValue)) +ThresholdMinValue, 0)  

		' Transfert in RandomValueIteration named cell
		Lig = oSheet.getCellRangeByName("RandomValueIteration").CellAddress.Row
		Col = oSheet.getCellRangeByName("RandomValueIteration").CellAddress.Column	 
		oSheet.getCellByPosition(Col + i, Lig).value		= RandomValue
		
		' Calculation and tranfert of Set point iteration		
		Lig = oSheet.getCellRangeByName("SetPointIteration").CellAddress.Row
		Col = oSheet.getCellRangeByName("SetPointIteration").CellAddress.Column	 
		
		oSheet.getCellByPosition(Col + i, Lig).value		= InitialSetPoint + RandomValue

		' Transfert in Time named cell
		Lig = oSheet.getCellRangeByName("TimeStepIteration").CellAddress.Row
		Col = oSheet.getCellRangeByName("TimeStepIteration").CellAddress.Column	 
		
		oSheet.getCellByPosition(Col + i, Lig).string		= Time  


		' Loop time management
		' Waiting duration base of 200 ms from the Wait instruction


' Waiting Duration according to TimeScale from the Drop Box

	Select Case TimeScale

		case 1
			WaitScale = 0	 		' Rafale, no waiting
			
		Case 2
			WaitScale = 1			' In seconds
			
		Case 3
			WaitScale = 60		' In minutes
			
		Case 4
			WaitScale = 3600	' In hours
					
	End Select 
	
	' Waiting Duration according to the mode: Rafale, seconds, minutes, hours 
	
	For t = 0 to WaitScale * 5 * TimeStepValue	

    	Wait 200

	Next t
		
	Next i	
		

End Sub


Sub StopProgramme()
' ============= Stop script button

	Stop

End Sub


Sub StopRAZ
' ============== Reset the iteration data and end the program
	
	' Reset the iteration data

	RAZIteration

	' Stop the script
	Stop

End Sub


Function RAZIteration()

' Erase all the iteration data. Can be simplified with a table

IterationNumber			= oSheet.getCellRangeByName("IterationNumber").value			' Number of iteration

	For j = 0 to IterationNumber -1 
	
	Dim Lig, Col As Integer
	
		' Transfert values by named cells
		Lig = oSheet.getCellRangeByName("IValue").CellAddress.Row
		Col = oSheet.getCellRangeByName("IValue").CellAddress.Column	 
		oSheet.getCellByPosition(Col + j, Lig).value		= j
	
		
		Lig = oSheet.getCellRangeByName("RandomValueIteration").CellAddress.Row
		Col = oSheet.getCellRangeByName("RandomValueIteration").CellAddress.Column	 
		oSheet.getCellByPosition(Col + j, Lig).value		= 0
		
		Lig = oSheet.getCellRangeByName("TimeStepIteration").CellAddress.Row
		Col = oSheet.getCellRangeByName("TimeStepIteration").CellAddress.Column	 
		oSheet.getCellByPosition(Col + j, Lig).value		= 0
	
		Lig = oSheet.getCellRangeByName("MaxThresholdIteration").CellAddress.Row
		Col = oSheet.getCellRangeByName("MaxThresholdIteration").CellAddress.Column	 
		oSheet.getCellByPosition(Col + j, Lig).value		= ThresholdMaxValue
			 
		Lig = oSheet.getCellRangeByName("MinThresholdIteration").CellAddress.Row
		Col = oSheet.getCellRangeByName("MinThresholdIteration").CellAddress.Column	 
		oSheet.getCellByPosition(Col + j, Lig).value		= ThresholdMinValue
			 
		 Lig = oSheet.getCellRangeByName("SetPointIteration").CellAddress.Row
		Col = oSheet.getCellRangeByName("SetPointIteration").CellAddress.Column	 
		oSheet.getCellByPosition(Col + j, Lig).value		= 0
	
	Next j

End Function

