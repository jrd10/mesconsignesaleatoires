﻿Option VBASupport 1

REM  *****  BASIC  *****
' Update: July 4, 2022

' WattCoop 2 Temporel by José Relland. June 29, 2022
' Loop on all the 20 values

Sub Main

' Declaration and assigantion for cells Column K
Dim ctdAction						As Object			' 10, 4 ; K5 	; Last CalculationTime
Dim cRandomValue 			As Object			' 10, 5 ; k6	; Random value between threshold
Dim cTimeStepValue 			As Object			' 10, 6 ; k7 	; Time step Value
Dim cThMaxValue				As Object			' 10, 7 ; k8	; Max threshold

Dim cThMinValue				As Object			' 10, 9 ; k10	; Min threshold
Dim	cInitialSetPoint			As Object			' 10, 10 ; k11	; Initial Set Point
Dim cCalculationValue		As object			' 10, 11  ; K12	;Range L12:AE12


' Declaration and assigantion for cells other Column 
Dim cTimeScale					As Object 		' 12,3	; M4 : Raffale, seconde

Dim cDeltaSetPoint				As Object			' 10,10 	; L9:AE9 

Dim cWorkingSetPoint		As Object			'  ; L11:AE11  
						

Dim oSheet 						As Object

oSheet = ThisComponent.CurrentController.getActiveSheet()


ctdAction 				= oSheet.getCellByPosition(10, 4)
cRandomValue 		= oSheet.getCellByPosition(10, 5)

cTimeStepValue 		= oSheet.getCellByPosition(10, 6)
cTimeScale				= oSheet.getCellByPosition(12, 3)


cThMaxValue			= oSheet.getCellByPosition(10, 7)
cThMinValue			= oSheet.getCellByPosition(10, 9)

cInitialSetPoint 		= oSheet.getCellByPosition(10, 10)
cCalculationValue	= oSheet.getCellByPosition(10, 11)


' Value
Dim TimeStepValue			As Variant		' L7 (s, min, h)	
Dim RandomValue				As Variant		' L6

Dim ThMaxValue							As Integer		' High Threshold
Dim ThMinValue							As Integer		' High Threshold

Dim	DeltaSetPoint				As Variant		' Line 10   Ecart Consigne 		
Dim	InitialSetPoint				As Variant		' J10   ConsigneDebut 
Dim WorkingSetPoint			As Variant		' Line 12 ConsigneTravail 

Dim i 									As Integer		'Increment for all the values
Dim TimeScale					As Integer		' Selection
Dim TimeScaleValue			As Integer		' Value


' ~~~~~~~~ Écrire dans une cellule ~~~~~~~~~~
'	VariableCellule.Value	= Valeur de la variable
'	Exemple : 		cRandomValue.Value		= RandomValue

' ~~~~~~~~ Lire dans une cellule   ~~~~~~~~~~~
'	Valeur de la variable = 	VariableCellule.Value
'	 Exemple : 		 InitialSetPoint				= oSheet.getCellRangeByName("k12").Value


' ------- Action Date and Time of the calculaiton -----------

	tdAction = Now
	ctdAction.String								= tdAction

		' Loop time management

	TimeScale = cTimeScale.Value

	
	' Rafale, seconds, minutes, hours
	Select Case TimeScale

		case 1
			TimeScaleValue = 0
			
		Case 2
			TimeScaleValue = 1	
			
		Case 3
			TimeScaleValue = 60
			
		Case 4
			TimeScaleValue = 3600
					
	End Select
	
	' Get the TimeStepValue
	TimeStepValue		 = oSheet.getCellByPosition(10, 6).Value
	
	
	' Read the thredsolds 
	ThMaxValue					= cThMaxValue.Value	
	ThMinValue						= cThMinValue.Value	
	
	
	' ------- RAZ all values -----------
	
	For i = 0 to 19 
	 
	 oSheet.getCellByPosition(11 + i, 5).value		= 0
	 oSheet.getCellByPosition(11 + i, 6).value		= 0
	 oSheet.getCellByPosition(11 + i, 8).value 		= 0
	 oSheet.getCellByPosition(11 + i, 10).value 		= 0
	 ' oSheet.getCellByPosition(11 + i, 11).value 		= 0
	 
	 Next i
	 
	' ------- Temporal Motor -----------
	
	 ' Write new value
	 For i = 0 to 19 

		
		' RandomValue					= Round((rnd * (max-min)) + min)  
		RandomValue					= Round((rnd * (ThMaxValue - ThMinValue)) +ThMinValue, 0)  
	
		' Transfert in cRandomValue in rounded value
		oSheet.getCellByPosition(11 + i, 5).value		= RandomValue

		' ------- Delta time Point -----------
	 
		' See in Wait


		
		' ------- Set Point delta and Set Point -----------
	 
	 	DeltaSetPoint					= RandomValue
	 	
	 	cDeltaSetPoint 		= oSheet.getCellByPosition(11 + i, 8)
		cDeltaSetPoint.Value		= DeltaSetPoint
		
		
		' ------- Working set Point -----------
		
		InitialSetPoint					= cInitialSetPoint.value
	 	WorkingSetPoint				= InitialSetPoint + RandomValue   ' DeltaSetPoint
	 	
	 	cWorkingSetPoint 	= oSheet.getCellByPosition(11 + i, 10)
	 	cWorkingSetPoint.Value	= 	WorkingSetPoint	


	' ~~~~~~~~~ Calculation by formula in Cell ~~~~~~~~~~~~~~~~
	' Nothing. See later if automatisation needed
		
		
		

	
	
	' wait
		
		' Write TimeStepValue
		
		Dim time as integer
		
		Time  = TimeStepValue + (TimeStepValue * i) 

		
		oSheet.getCellByPosition(11 + i, 6).value	= Time 
		
	    Wait TimeStepValue * 1000 * TimeScaleValue
		
'			print TimeStepValue
		
	Next i		
	
	 
	

	
	
	' Program stop
	
	



End Sub



