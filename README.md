# MesConsignesAléatoires

Générer des valeurs aléatoires, soit en rafale ou dans le temps, pour générer des différentes consignes de travail entre deux bornes.
Programme en BASIC pour Calc de LibreOffice. Projet de transcription sur VBA pour Excel.
Contributions et forks attendus. 

# Les versions

1. Premier version du programme réalisée pour Calc 
    * Fichier Calc (.ods) et le script BASIC (.bas) publié en v0.1) (02/08/2022)

    * À faire (02/08/2022)
        * Permettre des consigne basses positives
        * Travail sur des noms de cellule plutôt que leurs coordonnées (déplacement des tableaux)
        * Création dynamique du tableau
        * Avoir plusieurs niveaux de consignes aléatoires (qui peuvent dépendre d'une consigne précédente) 

2. Version v0.2
    * Tous les boutons faits
    * Cellules nommées faites. Le tableau est reconfigurables selon les cellules nommées
    * Pas de création dynamique du tableau. Je ne crois plus que cela soit utile :).
    * Reste à voir pour l'avenir plusieurs niveaux de consignes.
    * BUG : si on clique plusieurs fois sur le bouton Automatique (+3 fois), il passe en mode rafale ! Ensuite, cela redevient normal. Pas ce problème si l'on passe par le bouton Arrêt. Pour traiter ce pb j'ai déclenché un arrêt de programme sur la pression du bouton de la souris et le lancement du programme sur le relaché du bouton. Cela a réduit le défaut (?), mais il subsiste encore ce pb !



3. Transcription en VBA recherchée
    * Nécessite une réécriture en VBA ! 
    (Je n'ai pas trouvé d'outils de conversion dans ce sens mais j'ai vu cet outil VBA => OpenOffice BASIC https://www.business-spreadsheets.com/vba2oo.asp)


3. Correction de bugs et amélioration

## Quelques ressources en BASIC de LibreOffice

_Pour mémoire, BASIC est pour LibreOffice (OpenOffice...) ce que VBA est pour Excel. La programmation BASIC ne fonctionnent pas sur Excel, même si il existe un moyen de faire fonctionner des instructions VBA dans BASIC. Par contre, la programmation VBA fonctionne sur LibreOffice._  

* Aide sur LibreOffice BASIC (Wiki en EN) : https://help.libreoffice.org/Basic/Basic_Help

* Aide LibreOffice BASIC (FR) : https://help.libreoffice.org/latest/fr/text/sbasic/shared/main0601.html?DbPAR=BASIC

* les antisèches (FR) : https://wiki.documentfoundation.org/FR/Documentation/Publications#Antis.C3.A8ches_Basic

* Un tuto qui m'a été utile pour débuter : https://www.debugpoint.com/libreoffice-basic-macro-tutorial-index/




## License
Apache License, Version 2.0, January 2004


## English
Should you need more information, please let me know here, using Issues.


